package stepDefinitions;


import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import propertyPortalPages.*;

import java.net.MalformedURLException;

import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

public class RegisterNewUserAndSearchAProperty
{

    WebDriver driver;
    HomePage homePage;
    RegistrationPage registrationPage;
    LoginPage loginPage;
    SearchResultPage searchResultPage;
    PropertyDetails propertyDetails;


    @Before
    public void setUp()
    {
        driver = new ChromeDriver();
        homePage = PageFactory.initElements(driver, HomePage.class);
    }

    @After
    public void tearDown()
    {
        driver.close();
    }

/*
=======GIVEN=======
 */
    @Given("^I navigate to \"([^\"]*)\"$")
    public void i_navigate_to(String page) throws MalformedURLException
    {
        driver.get(page);
    }

    @Given("^I am a registered user and logged$")
    public void i_am_a_registered_user_and_logged() throws MalformedURLException
    {
        registerAUser();
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.performLogin();
    }

/*
=======WHEN=======
 */

    @When("^I press the Register link$")
    public void i_press_the_Register_link()
    {
        homePage.clickRegisterButton();
    }

    @When("^I search for the most expesive house in \"([^\"]*)\"$")
    public void i_search_for_the_most_expesive_house_in(String page) throws MalformedURLException
    {
        searchResultPage = PageFactory.initElements(driver, SearchResultPage.class);
        driver.get(page);
        searchResultPage.selectMostExpensiveOnesFirst();
        searchResultPage.selectTheFirstOfTheList();

    }

    @And("^I send a comment$")
    public void i_send_a_comment()
    {
        propertyDetails = PageFactory.initElements(driver, PropertyDetails.class);
        propertyDetails.submitComment();
    }

/*
=======THEN=======
 */

    @Then("^I navigate to the Registration form$")
    public void i_navigate_to_the_Registration_form()
    {
        registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        registrationPage.fillForm();
    }

    @Then("^I can see that one more person is interested$")
    public void i_can_see_that_one_more_person_is_interested()
    {
        driver.navigate().back();
        assertTrue("Nobody is interested", searchResultPage.isSomebodyInterested());
    }

    @And("^I can create a new account$")
    public void i_can_create_a_new_account()
    {
        registrationPage.sendForm();
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        assertTrue("Your account was not created", loginPage.isTheLoginPage());
    }

    private void registerAUser() throws MalformedURLException
    {
        i_navigate_to("https://grup5web.firebaseapp.com");
        i_press_the_Register_link();
        i_navigate_to_the_Registration_form();
        registrationPage.sendForm();
    }

}
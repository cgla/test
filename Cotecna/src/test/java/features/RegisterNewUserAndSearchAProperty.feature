Feature: Register and look for a new home

  Scenario: Register a new User
    Given I navigate to "https://grup5web.firebaseapp.com/"
    When I press the Register link
    Then I navigate to the Registration form
    And I can create a new account


   Scenario: A registered user search a home and write a comment
   Given I am a registered user and logged
   When I search for the most expesive house in "https://grup5web.firebaseapp.com/properties/properties.html?region=Navarra"
   And I send a comment
   Then I can see that one more person is interested


package propertyPortalPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

    @FindBy(xpath = "//input[@value='Conectarse']") WebElement loginButton;
    @FindBy(id = "username") WebElement username;
    @FindBy(id = "password") WebElement password;

    public boolean isTheLoginPage()
    {
        return loginButton.isDisplayed();
    }

    public void performLogin()
    {
        username.sendKeys("clatorre");
        password.sendKeys("AB12345678");
        loginButton.click();
    }
}

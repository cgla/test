package propertyPortalPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PropertyDetails {
    @FindBy(id = "comments") WebElement comments;
    @FindBy(xpath = "//input[@value='Enviar']") WebElement submitButton;

    public void submitComment()
    {
        comments.sendKeys("Bla bla bla");
        submitButton.click();
    }
}

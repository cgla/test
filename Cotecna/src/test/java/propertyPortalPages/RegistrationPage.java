package propertyPortalPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage {

    @FindBy(id = "name") WebElement name;
    @FindBy(id = "lastname") WebElement lastname;
    @FindBy(id = "dni") WebElement dni;
    @FindBy(id = "birthdate") WebElement birthdate;
    @FindBy(id = "address") WebElement address;
    @FindBy(id = "city") WebElement city;
    @FindBy(id = "zipcode") WebElement zipcode;
    @FindBy(id = "country") WebElement country;
    @FindBy(id = "phone") WebElement phone;
    @FindBy(id = "email") WebElement email;
    @FindBy(id = "username") WebElement username;
    @FindBy(id = "password") WebElement password;
    @FindBy(id = "verifyPassword") WebElement verifyPassword;
    @FindBy(xpath = "//input[@value='Registrarme']") WebElement registerButton;


    public void fillForm()
    {
        name.sendKeys("Cesar");
        lastname.sendKeys("Latorre");
        dni.sendKeys("42340776F");
        birthdate.sendKeys("2604193");
        address.sendKeys("soweto");
        city.sendKeys("Barcelona");
        zipcode.sendKeys("08038");
        country.sendKeys("España");
        phone.sendKeys("666666666");
        email.sendKeys("cesarlatorre@gmail.com");
        username.sendKeys("clatorre");
        password.sendKeys("AB12345678");
        verifyPassword.sendKeys("AB12345678");
    }

    public void sendForm()
    {
        registerButton.click();
    }
}

package propertyPortalPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage {

    @FindBy(id = "filter") WebElement filter;
    @FindBy(id = "likes-9") WebElement interestedPerson;
    @FindBy(xpath = "//h2[@id='price-9']") WebElement card_9;
    @FindBy(xpath = "//option[@value='mostExpensive']") WebElement optionMostExpensive;

    public void selectMostExpensiveOnesFirst()
    {
        filter.click();
        optionMostExpensive.click();
    }

    public void selectTheFirstOfTheList()
    {
        card_9.click();
    }

    public boolean isSomebodyInterested()
    {
        return interestedPerson.getText().equalsIgnoreCase("1 interesados");
    }
}
